# Webform To Leads

This module allows Webform submissions to be sent to Salesforce.

## Requirements

The Webform To Leads module requires Webform.

* [Webform](https://drupal.org/project/webform)

## Installation

Install this module with Composer.

```
composer require drupal/webform_to_leads
```

Enable this module with Drush.

```
drush en webform_to_leads
```

## Configuration

Define Salesforce OID, URL, and Field Mapping values at [Configuration > Web Services > Webform To Leads Settings](`/admin/config/services/web2lead/settings`).

## Maintainers

* [martin_t](https://www.drupal.org/u/martin_t)
* [bygeoffthompson](https://www.drupal.org/u/bygeoffthompson)